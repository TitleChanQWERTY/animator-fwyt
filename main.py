import pygame
from animator import Animator
pygame.init()

sc = pygame.display.set_mode((700, 650))

filename = ("SPRITE/1.png", "SPRITE/Angry.png", "SPRITE/2.png")

anim = Animator(10, filename)

image = anim.update()

clock = pygame.time.Clock()

while 1:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            pygame.quit()

    sc.fill((0, 0, 0))
    image = anim.update()
    image = pygame.transform.scale(image, (128, 128))
    sc.blit(image, image.get_rect(center=(550, 450)))
    pygame.display.update()
    clock.tick(60)
